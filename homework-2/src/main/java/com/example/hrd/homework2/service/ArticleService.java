package com.example.hrd.homework2.service;



import com.example.hrd.homework2.model.Article;

import java.util.List;

public interface ArticleService {
    List<Article> getAllArticles();
    Article findArticleById(int id);
    Article deleteArticleById(int id);
    void addArticle(Article article);
    void editArticle(Article article);
}
