package com.example.hrd.homework2.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Article {
    private int id;
    private String title;
    private String description;
    private String imageURL;
}
