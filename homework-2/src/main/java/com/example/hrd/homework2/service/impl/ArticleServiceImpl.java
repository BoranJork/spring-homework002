package com.example.hrd.homework2.service.impl;


import com.example.hrd.homework2.model.Article;
import com.example.hrd.homework2.service.ArticleService;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
public class ArticleServiceImpl implements ArticleService {

    List<Article> articles = new ArrayList<>();

    public ArticleServiceImpl() {
        for (int i = 1; i <= 10; i++) {
            Article article = new Article();
            article.setId(i);
            article.setTitle("This is my cute kitty "+i);
            article.setDescription("Life is better when you are near me <my Kitty> ");
            article.setImageURL("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR5O-Bsn0U47O-_QjOAXiJbRAiFTc0guJL-3g&usqp=CAU");
            articles.add(article);
        }
    }

    @Override
    public List<Article> getAllArticles() {
        return articles;
    }

    @Override
    public Article findArticleById(int id) {
        for (Article article : articles) {
            if (article.getId() == id) {
                return article;
            }
        }
        return null;
    }

    @Override
    public Article deleteArticleById(int id) {
        Article foundArticle = this.findArticleById(id);
        articles.removeIf(article -> article.getId() == id);
        return foundArticle;
    }

    @Override
    public void addArticle(Article article) {
        article.setId(this.articles.size() + 1);
        this.articles.add(article);
    }

    @Override
    public void editArticle(Article article) {
        int index = -1;
        for (int i = 0; i < this.articles.size(); i++) {
            if (articles.get(i).getId() == article.getId()) {
                index = i;
                break;
            }
        }
        articles.set(index, article);
    }
}
